import { Hero } from "./hero";


export const HEROES: Hero[] = [
    {
        id: 12, name: 'Dr. Nice'
    },
    {
        id: 13, name: 'Dr. Nice'
    },
    {
        id: 14, name: 'Dr. Nice'
    },
    {
        id: 15, name: 'Bombasito'
    },
    {
        id: 16, name: 'Dr. Magnet'
    },
    {
        id: 17, name: 'Dr. RubberMan'
    },
    {
        id: 18, name: 'Dr. IG'
    },
    {
        id: 19, name: 'Magma'
    },
    {
        id: 20, name: 'Tornado'
    }
]